function loadEvents() {
    loadFichajesJSON();
}

function getUserId() {
    var queryString = window.location.search;
    var posIgual = queryString.indexOf("=");
    var user_id = queryString.slice(posIgual + 1, queryString.length);

    return user_id;
} 

function loadFichajesJSON() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = processFichajes;
    xhr.open("GET", "http://localhost:5001/api/fichajes/" + getUserId(), true);
    xhr.send();
}

function processFichajes() {
    if (this.readyState == 4 && this.status == 200) {

        e = document.getElementById("fichajes");

        var obj = JSON.parse(this.responseText);

        if (obj.length > 0) {
            for (let i = 0; obj.length > i; i++) {

                let fecha = obj[i].date;
                var tipo = obj[i].tipo;
                var altitud = obj[i].pos.altitud;
                var latitud = obj[i].pos.latitud;

                //Slice fecha 
                let posData = fecha.indexOf("/");
                var dia = fecha.slice(0, posData);
                fecha = fecha.substring(posData + 1, fecha.length);

                posData = fecha.indexOf("/");
                var mes = fecha.slice(0, posData);
                fecha = fecha.substring(posData + 1, fecha.length);

                posData = fecha.indexOf(" ");
                var año = fecha.slice(0, posData);
                fecha = fecha.substring(posData + 1, fecha.length);

                posData = fecha.indexOf(":");
                var hora = fecha.slice(0, posData);
                fecha = fecha.substring(posData + 1, fecha.length);

                var minutos = fecha.slice(0, fecha.length);

                if (minutos < 10) {
                    minutos = 0 + minutos;
                }

                e.innerHTML += "<tr>" +
                    "<td>" + tipo + "</td>" +
                    "<td>" + dia + "</td>" +
                    "<td>" + mes + "</td>" +
                    "<td>" + año + "</td>" +
                    "<td>" + hora + ":" + minutos + "</td>" +
                    "<td> <a href='http://localhost:5000/mapa?alt=" + altitud + "&lat=" + latitud + "'> <img src='../media/mapIcon.jpg' alt='imagen mapa' width='40px' height='30px'/> </a> </td>" +
                    "</tr>";
            }
        }
    }
}
