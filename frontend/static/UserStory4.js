const altitud = getAltitud();
const latitud = getLatitud();

function loadEvents() {
    loadMap();

    flying(altitud, latitud);
}
function loadMap() {

    mapboxgl.accessToken = 'pk.eyJ1IjoiY3Jpc3RpYW5yYW1pcmV6OTkiLCJhIjoiY2toNHBzbHgxMDBqMzJ2cDV0aTNraGo3YyJ9.0OsfMH_GY07BxQ7xks3dYA';
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v11',
        //Posicion de españa
        center: [-3.74922, 38.463667],
        zoom: 5.5

    });

    var marker = new mapboxgl.Marker({})
        .setLngLat([altitud, latitud])
        .addTo(map);

}
function flying(altitud, latitud) {
    map.flyTo({
        center: [altitud, latitud],
        zoom: 12,
        essential: true
    });
}

function getLatitud() {
    var queryString = window.location.search;
    var posIgual = queryString.indexOf("&");
    var latitud = queryString.slice(posIgual + 5, queryString.length);
    return latitud;
}
function getAltitud() {
    var queryString = window.location.search;
    var posIgual = queryString.indexOf("=");
    var posFinal = queryString.indexOf("&");
    var altitud = queryString.slice(posIgual + 1, posFinal);
    return altitud;
}
