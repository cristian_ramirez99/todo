from mongoengine import Document
from mongoengine import StringField, BooleanField


class User(Document):
    user_id = StringField(max_length=40, required=True, unique=True)
    password = StringField(min_length=6, max_length=40, required=True)
    role = BooleanField(required=True)

    def __unicode__(self):
        return self.user_id
