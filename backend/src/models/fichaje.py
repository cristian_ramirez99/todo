from mongoengine import Document
from mongoengine import StringField, BooleanField, ListField, ReferenceField, DecimalField


class Pos(Document):
    altitud = DecimalField()
    latitud = DecimalField()


class Fichaje(Document):
    user_id = StringField(max_length=40, required=True)
    fichaje_id = StringField()
    date = StringField()
    tipo = BooleanField()
    pos = ListField(ReferenceField(Pos))
